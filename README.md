# Sovereign
SovereignSql is a database administration tool which supports any database engine which has a JDBC driver.

## Features

 - can connect to any database which has a JDBC driver
 - can create/edit/delete connections
 - can see the details of a database on a sidebar panel:
   * schemas
   * tables
   * table columns
   * views
   * functions
   * triggers
   * indexes
   * sequences
   * users
   * tablespaces
 - a click on any item from the previous list results in a refresh on the main panel displaying
   the details of the item **(in the first version tables and table columns are supported)**
 - clicking on a table results in the listing of its items (lazy sequence with scrolling)
 - can edit items
 - editing a foreign key results in a combo with all possible values
 - there is also a button next to the combo which opens a dialog with the list of possible values
   and the option to filter/create new entries for the referenced table
   * this function can be used recursively
 - details of the main panel:
   * checkbox for each row
   * add function
   * delete function (works with selected rows)
   * editing works simply by clicking on any cell
   * commit function
   * select all function
   * each row has a delete icon at the end
 - at the bottom there is an sql editor panel which can execute arbitrary sql
 - both main and sql editor panels support multiple tabs
 - context menu on the sidebar panel for
   * open in new tab
   * open in current tab
 - there is an sql log button at the bottom which opens up the sql log

   
## Roadmap:
 - delete (if applicable) option in sidebar context menu
 - new (if applicable) option in sidebar context menu

## License
SovereignSql is made available under the [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

## Credits
SovereignSql is created and maintained by Adam Arold and Robert Csakany
